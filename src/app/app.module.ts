import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { SistemaModule } from './sistema/sistema.module';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    AppRoutingModule,
    SistemaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
