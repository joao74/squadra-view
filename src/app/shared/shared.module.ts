import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ProgressBarModule } from 'primeng/progressbar';

@NgModule({
  exports: [
    HttpClientModule,
    FormsModule,
    ProgressBarModule,
    ReactiveFormsModule,
    CommonModule,
    PanelModule,
    FieldsetModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    ToastModule,
    MessagesModule,
    MessageModule
  ]
})
export class SharedModule { }
