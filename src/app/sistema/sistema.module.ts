import { NgModule } from '@angular/core';
import { SistemaRoutingModule } from './sistema-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SistemaComponent } from './components/sistema/sistema.component';
import { SistemaCadastrarComponent } from './components/sistema-cadastrar/sistema-cadastrar.component';
import { MessageService } from 'primeng/api';


@NgModule({
  imports: [
    SharedModule,
    SistemaRoutingModule
  ],
  declarations: [
    SistemaComponent,
    SistemaCadastrarComponent
  ],
  providers: [MessageService]
})
export class SistemaModule { }
