import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Sistema } from '../../sistema';
import { SistemaServiceService } from '../../sistema-service.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-sistema-cadastrar',
  templateUrl: './sistema-cadastrar.component.html',
  styleUrls: ['./sistema-cadastrar.component.css']
})
export class SistemaCadastrarComponent implements OnInit {

  /**
   * @var sistemaForm
   */
  public sistemaForm: FormGroup;

  /**
   * @var sistema
   */
  public sistema: Sistema;

  /**
   * @var carregando
   */
  public carregando = false;

  constructor(
    private formBuilder: FormBuilder,
    private sistemaService: SistemaServiceService,
    private route: Router,
    private messageService: MessageService) {
    this.sistema = new Sistema();
  }

  ngOnInit() {
    this.inicializaFormulario();
  }

  /**
   * Inicializa o formulário de manter sistema com as validações de campos necessárias.
   */
  public inicializaFormulario(): void {
    this.sistemaForm = this.formBuilder.group({
      descricao: ['', [Validators.required, Validators.maxLength(100)]],
      sigla: ['', [Validators.required, Validators.maxLength(10)]],
      emailAtendimento: ['', [Validators.email, Validators.maxLength(100)]],
      url: ['', Validators.maxLength(50)]
    });
  }

  /**
   * Metódo de submissão do formulário de manter sistema.
   */
  public salvarSistema(): void {
    this.carregando = true;

    this.sistemaService.salvar(this.sistemaForm.value).subscribe(response => {
      this.route.navigate(['/sistema']);
      this.messageService.add({severity: 'success', summary: 'Operação realizada com sucesso'});
      this.carregando = false;
    }, error => {
      this.carregando = false;
      this.messageService.add({severity: 'error', summary: error.error.message});
    });
  }

  /**
   * Cria uma propriedade do tipo FormControl para o campo 'descricao'.
   */
  get descricao(): FormControl {
    return <FormControl> this.sistemaForm.get('descricao');
  }

  /**
   * Cria uma propriedade do tipo FormControl para o campo 'sigla'.
   */
  get sigla(): FormControl {
    return <FormControl> this.sistemaForm.get('sigla');
  }

  /**
   * Cria uma propriedade do tipo FormControl para o campo 'emailAtendimento'.
   */
  get emailAtendimento(): FormControl {
    return <FormControl> this.sistemaForm.get('emailAtendimento');
  }
}
