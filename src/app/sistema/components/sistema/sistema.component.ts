import { Component, OnInit } from '@angular/core';
import { Sistema } from '../../sistema';
import { SistemaServiceService } from '../../sistema-service.service';
import { MessageService } from 'primeng/api';
import * as $ from 'jquery';

@Component({
  templateUrl: './sistema.component.html',
  styleUrls: ['./sistema.component.css']
})
export class SistemaComponent implements OnInit {

  /**
   * @var sistema
   */
  public sistemas: Sistema[];

  /**
   * @var filtro
   */
  public filtro: Sistema;

  /**
   * @var carregando
   */
  public carregando = false;

  constructor(
    private sistemaService: SistemaServiceService,
    private messageService: MessageService
  ) {
    this.filtro = new Sistema();
  }

  ngOnInit(): void {
    // TODO
  }

  /**
   * Limpa os campos na tela.
   */
  public limparCampos(): void {
    window.location.reload();
  }

  /**
   * Busca os sistemas por filtro quando houver o mesmo, caso contrário busca todos.
   */
  public buscarSistemasFiltro() {
    this.carregando = true;

    const operacao = (this.filtro)
    ? this.sistemaService.buscarSistemas(this.filtro)
    : this.sistemaService.buscarTodos();

    operacao.subscribe(response => {
      this.carregando = false;
      this.sistemas = response.data;
      $('#resultadoConsulta').show();
    }, error => {
      this.carregando = false;
      $('#resultadoConsulta').hide();
      this.messageService.add({severity: 'error', summary: error.error.message});
    });
  }
}
