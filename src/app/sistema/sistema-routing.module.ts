import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SistemaComponent } from './components/sistema/sistema.component';
import { SistemaCadastrarComponent } from './components/sistema-cadastrar/sistema-cadastrar.component';

const routes: Routes = [
  { path: 'sistema', component: SistemaComponent },
  { path: 'sistema/cadastrar', component: SistemaCadastrarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SistemaRoutingModule { }
