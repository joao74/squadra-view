import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sistema } from './sistema';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SistemaServiceService {

  constructor(private http: HttpClient) { }

  /**
   * Busca todos os sistemas cadastrados por filtro.
   *
   * @param filtro
   */
  public buscarSistemas(filtro): Observable<any> {
    return this.http.get(environment.API_URL + '/pesquisar', {params: filtro});
  }

  /**
   * Informa as informações necessárias para salvar um sistema.
   *
   * @param sistema
   */
  public salvar(sistema: Sistema): Observable<any> {
    return this.http.post(environment.API_URL, sistema).pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  /**
   * Busca todos os sistemas cadastrados.
   */
  public buscarTodos(): Observable<Sistema[]> {
    return this.http.get<Sistema[]>(environment.API_URL);
  }
}
