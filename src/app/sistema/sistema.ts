export class Sistema {
  descricao: string;
  sigla: string;
  emailAtendimento: string;
  url: string;
  status: string;
}
